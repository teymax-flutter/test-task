import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_weather_bg/flutter_weather_bg.dart';
import 'package:test_task/screens/cities_screen.dart';
import 'package:test_task/services/networking.dart';

class LoadingScreen extends StatefulWidget {
  @override
  _LoadingScreenState createState() => _LoadingScreenState();
}

class _LoadingScreenState extends State<LoadingScreen> {
  void getCities() async {
    NetworkHelper networkHelper = NetworkHelper();
    var citiesData = await networkHelper.parseCitiesJson();
    Navigator.pushReplacement(
      context,
      MaterialPageRoute(
        builder: (context) => CitiesScreen(
          citiesData: citiesData,
        ),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    getCities();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Center(
            child: WeatherBg(
              weatherType: WeatherType.middleRainy,
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
            ),
          ),
          Container(
            // decoration: BoxDecoration(
            //   image: DecorationImage(
            //     alignment: Alignment.bottomCenter,
            //     scale: 0.1,
            //     //fit: BoxFit.contain,
            //     image: AssetImage('images/ManUmb.png'),
            //   ),
            // ),
            child: Align(
              alignment: Alignment.bottomCenter,
              child: Image.asset(
                'images/ManUmb.png',
                height: 300,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
