import 'package:flutter/material.dart';
import 'package:test_task/screens/search_screen.dart';
import 'package:test_task/screens/weather_screen.dart';
import 'package:test_task/services/weather.dart';

class CitiesScreen extends StatelessWidget {
  final dynamic citiesData;

  CitiesScreen({this.citiesData});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Text('Weather App'),
        leading: FlatButton(
          child: Hero(
            tag: 'search',
            child: Icon(
              Icons.search,
              color: Colors.white,
            ),
          ),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => SearchScreen(),
              ),
            );
          },
        ),
      ),
      body: ListView.builder(
        itemBuilder: (context, index) {
          return ListTile(
            title: Text(citiesData[index]['name']),
            onTap: () async {
              var model = WeatherModel();
              var weatherData =
                  await model.getWeatherByCityId(citiesData[index]['id']);
              var forecastData = await model.getWeatherByCoordinates(
                latitude: citiesData[index]['coord']['lat'],
                longitude: citiesData[index]['coord']['lon'],
              );
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => WeatherScreen(
                    weatherData: weatherData,
                    forecastData: forecastData,
                  ),
                ),
              );
            },
          );
        },
      ),
    );
  }
}
