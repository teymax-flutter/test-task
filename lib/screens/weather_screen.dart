import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:test_task/services/weather.dart';
import 'package:test_task/widgets/hourly_forecast_info.dart';
import 'package:test_task/widgets/weather_chart.dart';
import 'package:test_task/widgets/weather_info_field.dart';

class WeatherScreen extends StatefulWidget {
  final weatherData;
  final forecastData;

  WeatherScreen({@required this.weatherData, this.forecastData});

  @override
  _WeatherScreenState createState() => _WeatherScreenState();
}

class _WeatherScreenState extends State<WeatherScreen> {
  String countryShort = '';
  String cityName = '[City]';
  int temperature = 0;
  String weatherDescription =
      'Error. You need internet connection to make this work.';
  int realfeelTemp = 0;
  int windSpeed = 0;
  int humidity = 0;
  int pressure = 0;
  //String iconMain = '';
  dynamic forecastData;

  @override
  void initState() {
    super.initState();
    updateUI(widget.weatherData, forecastData: widget.forecastData);
  }

  void updateUI(dynamic weatherData, {dynamic forecastData}) {
    setState(() {
      if (weatherData == null) return;

      cityName = weatherData['name'];
      countryShort = weatherData['sys']['country'];
      temperature = weatherData['main']['temp'].toInt();
      weatherDescription = weatherData['weather'][0]['main'];
      realfeelTemp = weatherData['main']['feels_like'].toInt();
      windSpeed = weatherData['wind']['speed'].toInt();
      humidity = weatherData['main']['humidity'];
      pressure = (weatherData['main']['pressure'].toInt() / 1.333).toInt();
      //iconMain = weatherData['weather'][0]['icon'];

      if (forecastData == null) return;
      this.forecastData = forecastData;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: ListView(
          //crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Container(
              child: SafeArea(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Container(
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          alignment: Alignment.topRight,
                          image: AssetImage('images/beacon.png'),
                          fit: BoxFit.contain,
                        ),
                      ),
                      padding: EdgeInsets.only(top: 50, left: 40, bottom: 30),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            padding: EdgeInsets.all(10),
                            decoration: BoxDecoration(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10)),
                              color: Color(0xfff5f5f5),
                            ),
                            child: RichText(
                              text: TextSpan(
                                style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 20,
                                ),
                                children: [
                                  WidgetSpan(
                                    child: Icon(
                                      Icons.near_me,
                                      size: 18,
                                    ),
                                  ),
                                  TextSpan(
                                    text: '  $cityName, $countryShort',
                                    style: TextStyle(
                                      fontSize: 15,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Text(
                            '$temperature°',
                            style: TextStyle(
                              fontSize: 100,
                            ),
                          ),
                          RichText(
                            text: TextSpan(
                              style: TextStyle(
                                color: Colors.blue[900],
                                fontSize: 20,
                              ),
                              children: [
                                WidgetSpan(
                                  child: Icon(
                                    Icons.filter_drama,
                                    color: Colors.blue[900],
                                  ),
                                ),
                                TextSpan(
                                  text: '  $weatherDescription',
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    Divider(
                      thickness: 1,
                      height: 1,
                    ),
                    Container(
                      padding: EdgeInsets.all(30),
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              WeatherInfoField(
                                parameterName: 'Realfeel',
                                parameterValue: '$realfeelTemp°',
                              ),
                              WeatherInfoField(
                                parameterName: 'Wind',
                                parameterValue: '$windSpeed m/s',
                              ),
                            ],
                          ),
                          Container(
                            margin: EdgeInsets.symmetric(vertical: 20),
                            child: Divider(
                              thickness: 1,
                              height: 1,
                            ),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              WeatherInfoField(
                                parameterName: 'Pressure',
                                parameterValue: '$pressure mmHg',
                              ),
                              WeatherInfoField(
                                parameterName: 'Humidity',
                                parameterValue: '$humidity%',
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    Divider(
                      thickness: 1,
                      height: 1,
                    ),
                    Container(
                      padding: EdgeInsets.all(20),
                      child: Center(
                        child: Text(
                          'Timezone - ${forecastData['timezone']}',
                          style: TextStyle(
                            color: Color(0xff00286b),
                            fontSize: 20,
                            fontWeight: FontWeight.w300,
                          ),
                        ),
                      ),
                    ),
                    Container(
                      height: 200,
                      padding: EdgeInsets.all(20),
                      child: ListView.builder(
                        shrinkWrap: true,
                        scrollDirection: Axis.horizontal,
                        itemBuilder: (context, index) {
                          DateTime time = DateTime.fromMillisecondsSinceEpoch(
                            forecastData['hourly'][index]['dt'] * 1000,
                            isUtc: true,
                          );
                          time = time.add(Duration(
                              seconds: forecastData['timezone_offset']));

                          // Printing some meta info
                          // print('${time.hour}:${time.minute}');
                          // print(forecastData['timezone']);
                          // print('Latitude: ${forecastData['lat']} && Longitude: ${forecastData['lon']}');
                          return HourlyForecastBlock(
                            time: '${time.hour.toString()}:00',
                            day: '${time.day}.${time.month}.${time.year}',
                            temperature:
                                forecastData['hourly'][index]['temp'].toInt(),
                            iconCode: forecastData['hourly'][index]['weather']
                                [0]['icon'],
                            textColor: index == 0
                                ? Color(0xff00286b)
                                : Color(0xffb0b2b5),
                          );
                        },
                        itemCount: 24,
                      ),
                    ),
                    WeatherChart(
                      forecastData['hourly'],
                      forecastData['timezone_offset'],
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
