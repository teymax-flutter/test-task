import 'package:flutter/material.dart';
import 'package:test_task/screens/weather_screen.dart';
import 'package:test_task/services/weather.dart';
import 'package:test_task/utilities/constants.dart';

class SearchScreen extends StatefulWidget {
  @override
  _SearchScreenState createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  String locationName;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        constraints: BoxConstraints.expand(),
        child: SafeArea(
          child: Column(
            children: <Widget>[
              Align(
                alignment: Alignment.topLeft,
                child: FlatButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Icon(
                    Icons.arrow_back_ios,
                    size: 50.0,
                    color: Colors.blue,
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.all(20.0),
                child: TextField(
                  style: TextStyle(
                    color: Colors.black,
                  ),
                  decoration: kTextFieldInputDecoration,
                  onChanged: (value) {
                    locationName = value;
                  },
                ),
              ),
              FlatButton(
                onPressed: () async {
                  var model = WeatherModel();
                  var weatherData = await model.getCityWeather(locationName);
                  var forecastData = await model.getWeatherByCoordinates(
                    latitude: weatherData['coord']['lat'],
                    longitude: weatherData['coord']['lon'],
                  );
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => WeatherScreen(
                        weatherData: weatherData,
                        forecastData: forecastData,
                      ),
                    ),
                  );
                },
                child: RichText(
                  text: TextSpan(
                    children: [
                      WidgetSpan(
                        child: Hero(
                          tag: 'search',
                          child: Icon(
                            Icons.search,
                            color: Colors.lightBlueAccent,
                            size: 30,
                          ),
                        ),
                      ),
                      TextSpan(
                        text: ' Get Weather',
                        style: kButtonTextStyle,
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
