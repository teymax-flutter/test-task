import 'package:flutter/cupertino.dart';
import 'package:weather_icons/weather_icons.dart';
import 'package:flutter/material.dart';
import 'networking.dart';

const apiKey = 'def888412b3b8e8ac6405d971823b095';
const openWeatherMapURL = 'https://api.openweathermap.org/data/2.5/weather';

class WeatherModel {
  Future<dynamic> getWeatherByCityId(int cityID) async {
    var url = '$openWeatherMapURL?id=$cityID&appid=$apiKey&units=metric';
    NetworkHelper networkHelper = NetworkHelper(url: url);
    var weatherData = await networkHelper.getData();
    return weatherData;
  }

  Future<dynamic> getCityWeather(String cityName) async {
    var url = '$openWeatherMapURL?q=$cityName&appid=$apiKey&units=metric';
    NetworkHelper networkHelper = NetworkHelper(url: url);
    var weatherData = await networkHelper.getData();
    return weatherData;
  }

  Future<dynamic> getWeatherByCoordinates(
      {@required double latitude, @required double longitude}) async {
    var url =
        'https://api.openweathermap.org/data/2.5/onecall?lat=$latitude&lon=$longitude&exclude=current,minutely,daily,alerts&appid=$apiKey&units=metric';
    NetworkHelper networkHelper = NetworkHelper(url: url);
    var weatherData = await networkHelper.getData();
    return weatherData;
  }

  static Icon getWeatherIcon(String iconCode, Color colour) {
    if (iconCode == '01d') {
      return Icon(
        WeatherIcons.day_sunny,
        color: colour,
      );
    } else if (iconCode == '01n') {
      return Icon(
        WeatherIcons.night_clear,
        color: colour,
      );
    } else if (iconCode == '02d') {
      return Icon(
        WeatherIcons.day_cloudy,
        color: colour,
      );
    } else if (iconCode == '02n') {
      return Icon(
        WeatherIcons.night_alt_cloudy,
        color: colour,
      );
    } else if (iconCode == '03d' || iconCode == '03n') {
      return Icon(
        WeatherIcons.cloud,
        color: colour,
      );
    } else if (iconCode == '04d' || iconCode == '04n') {
      return Icon(
        WeatherIcons.cloudy,
        color: colour,
      );
    } else if (iconCode == '09d' || iconCode == '09n') {
      return Icon(
        WeatherIcons.rain,
        color: colour,
      );
    } else if (iconCode == '10d') {
      return Icon(
        WeatherIcons.day_rain_mix,
        color: colour,
      );
    } else if (iconCode == '10n') {
      return Icon(
        WeatherIcons.night_rain_mix,
        color: colour,
      );
    } else if (iconCode == '11d' || iconCode == '11n') {
      return Icon(
        WeatherIcons.thunderstorm,
        color: colour,
      );
    } else if (iconCode == '13d' || iconCode == '13n') {
      return Icon(
        Icons.ac_unit,
        color: colour,
      );
    } else if (iconCode == '50d' || iconCode == '50n') {
      return Icon(
        WeatherIcons.fog,
        color: colour,
      );
    } else
      return null;
  }
}
