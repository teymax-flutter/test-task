import 'package:flutter/material.dart';
import 'package:test_task/services/weather.dart';

class HourlyForecastBlock extends StatelessWidget {
  final String time;
  final String day;
  final int temperature;
  final String iconCode;
  final Color textColor;

  HourlyForecastBlock({
    this.time,
    this.day,
    this.temperature,
    this.iconCode,
    this.textColor,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 20),
      child: Column(
        children: [
          Text(
            '$day',
            style: TextStyle(
              color: textColor,
              fontSize: 15,
            ),
          ),
          Text(
            '$time',
            style: TextStyle(
              color: textColor,
              fontSize: 15,
            ),
          ),
          SizedBox(
            height: 15,
          ),
          Text(
            '$temperature°',
            style: TextStyle(
              color: textColor,
              fontSize: 30,
            ),
          ),
          SizedBox(
            height: 15,
          ),
          WeatherModel.getWeatherIcon(iconCode, textColor),
        ],
      ),
    );
  }
}
