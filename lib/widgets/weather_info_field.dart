import 'package:flutter/material.dart';

class WeatherInfoField extends StatelessWidget {
  final String parameterName;
  final String parameterValue;

  WeatherInfoField(
      {@required this.parameterName, @required this.parameterValue});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(10),
      child: Row(
        children: [
          Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.baseline,
              textBaseline: TextBaseline.ideographic,
              children: [
                Text(
                  '$parameterName',
                  style: TextStyle(
                    color: Color(0xffa2a4a8),
                    fontSize: 15,
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  '$parameterValue',
                  style: TextStyle(
                    fontSize: 30,
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
