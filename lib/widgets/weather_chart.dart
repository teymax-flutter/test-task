import 'package:flutter/material.dart';
import 'package:fl_chart/fl_chart.dart';

class WeatherChart extends StatefulWidget {
  dynamic hourlyForecastData;
  int timezoneOffset;

  WeatherChart(this.hourlyForecastData, this.timezoneOffset);

  @override
  _WeatherChartState createState() => _WeatherChartState();
}

class _WeatherChartState extends State<WeatherChart> {
  List<Color> gradientColors = [
    const Color(0xff23b6e6),
    const Color(0xff02d39a),
  ];
  dynamic data;
  int _currentHour;

  int getShiftedHour(int hours) {
    int result = _currentHour + hours;
    if (result > 24) {
      result -= 24;
    }
    return result;
  }

  double getMinTemperature() {
    int minTemp = 100;
    for (int i = 0; i < 24; i++) {
      if (data[i]['temp'] < minTemp) minTemp = data[i]['temp'].toInt();
    }
    //print(minTemp);
    return minTemp.toDouble() - 1;
  }

  double getMaxTemperature() {
    int maxTemp = -100;
    for (int i = 0; i < 24; i++) {
      if (data[i]['temp'] > maxTemp) maxTemp = data[i]['temp'].toInt();
    }
    //print(maxTemp);
    return maxTemp.toDouble() + 1;
  }

  @override
  void initState() {
    super.initState();
    data = widget.hourlyForecastData;
    _currentHour = DateTime.fromMillisecondsSinceEpoch(
            (data[0]['dt'] + widget.timezoneOffset) * 1000,
            isUtc: true)
        .hour;
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        AspectRatio(
          aspectRatio: 1.70,
          child: Container(
            decoration: const BoxDecoration(
                // borderRadius: BorderRadius.all(
                //   Radius.circular(18),
                // ),
                color: Color(0xff232d37)),
            child: Padding(
              padding: const EdgeInsets.only(
                  right: 18.0, left: 12.0, top: 24, bottom: 12),
              child: LineChart(mainData()),
            ),
          ),
        ),
      ],
    );
  }

  List<FlSpot> _getChartSpots() {
    List<FlSpot> spots = [];
    for (int i = 0; i < 24; i++) {
      spots.add(
        FlSpot(
          i.toDouble(),
          data[i]['temp'].toDouble(),
        ),
      );
      //print('x = ${spots[i].x}  &&  y = ${spots[i].y}');
    }

    return spots;
  }

  LineChartData mainData() {
    return LineChartData(
      gridData: FlGridData(
        show: true,
        drawVerticalLine: true,
        getDrawingHorizontalLine: (value) {
          return FlLine(
            color: const Color(0xff37434d),
            strokeWidth: 1,
          );
        },
        getDrawingVerticalLine: (value) {
          return FlLine(
            color: const Color(0xff37434d),
            strokeWidth: 1,
          );
        },
      ),
      titlesData: FlTitlesData(
        show: true,
        bottomTitles: SideTitles(
          showTitles: true,
          reservedSize: 22,
          getTextStyles: (value) => const TextStyle(
              color: Color(0xff68737d),
              fontWeight: FontWeight.bold,
              fontSize: 10),
          getTitles: (value) {
            switch (value.toInt()) {
              case 0:
                return '$_currentHour:00';
              case 3:
                return '${getShiftedHour(3)}:00';
              case 6:
                return '${getShiftedHour(6)}:00';
              case 9:
                return '${getShiftedHour(9)}:00';
              case 12:
                return '${getShiftedHour(12)}:00';
              case 15:
                return '${getShiftedHour(15)}:00';
              case 18:
                return '${getShiftedHour(18)}:00';
              case 21:
                return '${getShiftedHour(21)}:00';
            }
            return '';
          },
          margin: 8,
        ),
        leftTitles: SideTitles(
          showTitles: true,
          getTextStyles: (value) => const TextStyle(
            color: Color(0xff67727d),
            fontWeight: FontWeight.bold,
            fontSize: 10,
          ),
          getTitles: (value) {
            switch (value.toInt()) {
              case -20:
                return '-20°';
              case -15:
                return '-15°';
              case -10:
                return '-10°';
              case -5:
                return '-5°';
              case 0:
                return '0°';
              case 5:
                return '5°';
              case 10:
                return '10°';
              case 15:
                return '15°';
              case 20:
                return '20°';
              case 25:
                return '25°';
              case 30:
                return '30°';
              case 35:
                return '35°';
              case 40:
                return '40°';
              case 45:
                return '45°';
            }
            return '';
          },
          reservedSize: 28,
          margin: 12,
        ),
      ),
      borderData: FlBorderData(
          show: true,
          border: Border.all(color: const Color(0xff37434d), width: 1)),
      minX: 0,
      maxX: 23,
      minY: getMinTemperature(),
      maxY: getMaxTemperature(),
      lineBarsData: [
        LineChartBarData(
          spots: _getChartSpots(),
          isCurved: true,
          colors: gradientColors,
          barWidth: 5,
          isStrokeCapRound: true,
          dotData: FlDotData(
            show: false,
          ),
          belowBarData: BarAreaData(
            show: true,
            colors:
                gradientColors.map((color) => color.withOpacity(0.3)).toList(),
          ),
        ),
      ],
    );
  }
}
